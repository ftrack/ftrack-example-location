#ftrack Example Location

.. warning::

    The code has been moved from this repository and now can be found among 
    others, in the `ftrack-recipes <https://bitbucket.org/ftrack/ftrack-recipes/>`_ repository: